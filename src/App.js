import './App.css';
import Header from './components/Header/Header.js';
import Profile from './components/Profile/Profile.js';
import Navbar from './components/Navbar/Navbar.js';
import DialogsContainer from './components/Dialogs/DialogsContainer.js'
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import FriendsContainer from './components/Friends/FriendsContainer';



const App = (props) => {
  return (
    <div className='app-wrapper'>
      <BrowserRouter>
        <Header />
        <Navbar />
        <div className='app-wrapper-content'>
          <Routes>
            <Route path='/profile' element={<Profile />} />
            <Route path='/dialogs/*' element={<DialogsContainer />} />
            <Route path='/friends' element={<FriendsContainer />} />
          </Routes>
        </div>
      </BrowserRouter>
    </div>
  );
}

export default App;
