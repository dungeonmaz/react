const ADD_POST = 'ADD-POST'
const CHANGE_NEW_POST_TEXT = 'CHANGE-NEW-POST-TEXT'

export const addPostActionCreator = () => {
    return {
        type: ADD_POST
    }
}

export const changeNewPostTextActionCreator = (text) => {
    return {
        type: CHANGE_NEW_POST_TEXT,
        newPostText: text
    }
}


let initialState = {
    newPostText: '',
    posts: [
        { id: 1, text: 'hi, abonga', likes: 16 },
        { id: 2, text: 'i am abunga', likes: 4 },
        { id: 3, text: 'adunga', likes: 48 },
        { id: 4, text: 'adun', likes: 456 },
    ],
}

const profileReducer = (state = initialState, action) => {
    let stateCopy = {...state}

    function _addPost() {
        stateCopy.posts = [...state.posts]
        let newPost = {
            id: 5,
            text: state.newPostText,
            likes: 0,
        }
        stateCopy.posts.push(newPost)
        stateCopy.newPostText = ''
    }

    function _changeNewPostText(newText) {
        stateCopy.newPostText = newText
    }

    switch (action.type) {
        case ADD_POST:
            _addPost()
            break
        case CHANGE_NEW_POST_TEXT:
            _changeNewPostText(action.newPostText)
            break
        default:
            break
    }

    return stateCopy
}

export default profileReducer