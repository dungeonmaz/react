import dialogsReducer from "./dialogsReducer"
import profileReducer from "./profileReducer"

let store = {
    _state: {
        profilePage: {
            newPostText: '',
            posts: [
                { id: 1, text: 'hi, abonga', likes: 16 },
                { id: 2, text: 'i am abunga', likes: 4 },
                { id: 3, text: 'adunga', likes: 48 },
                { id: 4, text: 'adun', likes: 456 },
            ],
        },

        dialogsPage: {
            newMessageText: '',
            dialogs: [
                { id: 1, name: 'Arthur' },
                { id: 2, name: 'Alisa' },
                { id: 3, name: 'Vika' },
                { id: 4, name: 'Denis' },
                { id: 5, name: 'Alexey' }
            ],

            messages: [
                { id: 1, text: 'Hi' },
                { id: 2, text: 'Yo' },
                { id: 3, text: 'Booba' },
                { id: 4, text: 'Go' },
                { id: 5, text: 'Abunga' },
            ],
        },
    },

    _callSubscriber() {
        console.log("No observers")
    },

    getState() {
        return this._state
    },

    subscribe(observer) {
        this._callSubscriber = observer
    },


    dispatch(action){
        this._state.profilePage = profileReducer(this._state.profilePage, action)
        this._state.dialogsPage = dialogsReducer(this._state.dialogsPage, action)
        this._callSubscriber(this._state)
    }

}

export default store;