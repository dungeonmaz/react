const ADD_MESSAGE = 'ADD-MESSAGE'
const CHANGE_NEW_MESSAGE_TEXT = 'CHANGE-NEW-MESSAGE-TEXT'

export const addMessageActionCreator = () => {
    return {
        type: ADD_MESSAGE
    }
}

export const changeNewMessageTextActionCreator = (text) => {
    return {
        type: CHANGE_NEW_MESSAGE_TEXT,
        newMessageText: text
    }
}

let initialState = {
    newMessageText: '',
    dialogs: [
        { id: 1, name: 'Arthur' },
        { id: 2, name: 'Alisa' },
        { id: 3, name: 'Vika' },
        { id: 4, name: 'Denis' },
        { id: 5, name: 'Alex' }
    ],

    messages: [
        { id: 1, text: 'Hi' },
        { id: 2, text: 'Yo' },
        { id: 3, text: 'Booba' },
        { id: 4, text: 'Go' },
        { id: 5, text: 'Abunga' },
    ],
}

const dialogsReducer = (state = initialState, action) => {
    let stateCopy = { ...state }

    function _addMessage() {
        stateCopy.messages = [...state.messages]
        let newMessage = {
            id: 6,
            text: state.newMessageText
        }
        stateCopy.messages.push(newMessage)
        stateCopy.newMessageText = ''
    }

    function _changeNewMessageText(newText) {
        stateCopy.newMessageText = newText
    }

    switch (action.type) {
        case ADD_MESSAGE:
            _addMessage()
            break
        case CHANGE_NEW_MESSAGE_TEXT:
            _changeNewMessageText(action.newMessageText)
            break
        default:
            break
    }

    return stateCopy
}

export default dialogsReducer