const ADD_REMOVE_FRIEND = 'ADD-REMOVE-FRIEND'

export const addRemoveFriendActionCreator = (id) =>{
    return{
        type: ADD_REMOVE_FRIEND,
        id: id
    }
}

let initialState = {
    friends:[
        { id: 0, name: 'Arthur', isFriend: true, info: 'Abingus', location: {country: 'Russia', city: 'Moscow'} },
        { id: 1, name: 'Alisa', isFriend: true, info: 'Abunga', location: {country: 'Russia', city: 'Novosibirsk'} },
        { id: 2, name: 'Alex', isFriend: false, info: 'Info', location: {country: 'Russia', city: 'Tomsk'} },
        { id: 3, name: 'Jojo', isFriend: false, info: 'Based', location: {country: 'China', city: 'Shenzhen'} },
    ]
}

const friendsReducer = (state = initialState, action) => {
    let stateCopy = {...state}

    function _addRemoveFriend(id){
        stateCopy.friends = [...state.friends]
        stateCopy.friends[id].isFriend = stateCopy.friends[id].isFriend ^ true
        return stateCopy
    }

    switch(action.type){
        case ADD_REMOVE_FRIEND:
            _addRemoveFriend(action.id)
            break
        default:
            break
    }

    return stateCopy
}

export default friendsReducer