import classes from './Header.module.css'

const Header = () => {
    return (
        <header className={classes.header}>
            <img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTKOQkr1mw12Hw6F3DCtuBPW7Xl5lvo7rYiuXpyz34yd3_Yn6eLB7ATcZuIkYAyzrG8WGM&usqp=CAU'></img>
        </header>
    )
}

export default Header;