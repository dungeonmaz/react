import classes from './Navbar.module.css'
import { NavLink } from 'react-router-dom';


const Navbar = () => {
    return (
        <nav className={classes.nav}>

            <div className={classes.item}>
                <NavLink to="/profile" style={({isActive}) =>({
                    color: isActive ? 'gold' : ''
                })}>Profile</NavLink>
            </div>

            <div className={classes.item} >
                <NavLink to="/dialogs" style={({isActive}) =>({
                    color: isActive ? 'gold' : ''
                })}>Messages</NavLink>
            </div>

            <div className={classes.item} >
                <NavLink to="/friends" style={({isActive}) =>({
                    color: isActive ? 'gold' : ''
                })}>Friends</NavLink>
            </div>

        </nav>
    )
}

export default Navbar;