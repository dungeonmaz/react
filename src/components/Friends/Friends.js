import FriendItemContainer from "./FriendItem/FriendItemContainer"

const Friends = (props) => {
    let friendElements = props.friendsPage.friends.map(friend => <FriendItemContainer name={friend.name}
        id={friend.id} isFriend={friend.isFriend} info = {friend.info} location = {friend.location} />)
    return (
        <div>
            {friendElements}
        </div>
    )
}

export default Friends