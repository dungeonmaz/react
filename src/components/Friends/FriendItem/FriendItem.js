import classes from './FriendItem.module.css'

const FriendItem = (props) => {
    let onAddRemoveFriend = () => {
        props.addRemoveFriend(props.id)
    }
    return (
        <div className={classes.friend}>
            <div className={classes.text}>
                <div className={classes.name}>{props.name} <button onClick={onAddRemoveFriend}>{props.isFriend ? "Y" : "N"}</button></div>
                <div className={classes.info}>{props.info}</div>
            </div>
            <div className={classes.imageLoc}>
                <img src='https://thumbs.dreamstime.com/b/illustration-profile-icon-avatar-inhabitant-male-illustration-profile-icon-avata-237916010.jpg'></img>
                <div className={classes.location}>
                    <div>{props.location.country}</div>
                    <div>{props.location.city}</div>
                </div>
            </div>
        </div>
    )
}

export default FriendItem