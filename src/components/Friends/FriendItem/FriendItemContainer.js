import { connect } from "react-redux"
import { addRemoveFriendActionCreator } from "../../../redux/friendsReducer"
import FriendItem from "./FriendItem"

let mapStateToProps = (state) => {
    return {
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        addRemoveFriend: (id) => {
            let action = addRemoveFriendActionCreator(id)
            dispatch(action)
        }
    }
}

const FriendItemContainer = connect(mapStateToProps, mapDispatchToProps)(FriendItem)

export default FriendItemContainer