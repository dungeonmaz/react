import classes from './ProfileInfo.module.css'

const ProfileInfo = () => {
    return (
        <div className={classes.profileImage}>
            <img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTKOQkr1mw12Hw6F3DCtuBPW7Xl5lvo7rYiuXpyz34yd3_Yn6eLB7ATcZuIkYAyzrG8WGM&usqp=CAU'></img>
        </div>
    )
}

export default ProfileInfo;