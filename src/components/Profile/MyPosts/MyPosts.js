import { createRef } from 'react';
import classes from './MyPosts.module.css'
import Post from './Post/Post.js';


const MyPosts = (props) => {

    let postsElements = props.profilePage.posts.map(post => <Post message={post.text} likes={post.likes} />)

    let onAddPost = () => {
        props.addPost()
    }

    let newPostElement = createRef();

    let onPostChange = () => {
        let text = newPostElement.current.value;
        props.changeNewPostText(text)
    }

    return (
        <div className={classes.postsBlock}>
            <h2>Posts</h2>
            <div>
                <textarea ref={newPostElement} value={props.profilePage.newPostText} onChange={onPostChange} />
                <div>
                    <button onClick={onAddPost}>Add post</button>
                </div>
            </div>
            <div className={classes.posts}>
                {postsElements}
            </div>
        </div>

    )
}


export default MyPosts;