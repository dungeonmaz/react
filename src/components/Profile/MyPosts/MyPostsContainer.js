import { connect } from 'react-redux';
import { addPostActionCreator, changeNewPostTextActionCreator } from '../../../redux/profileReducer';
import MyPosts from './MyPosts';


let mapStateToProps = (state) => {
    return {
        profilePage: state.profilePage
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        changeNewPostText: (text) => {
            let action = changeNewPostTextActionCreator(text)
            dispatch(action)
        },

        addPost: () => {
            let action = addPostActionCreator()
            dispatch(action)
        }
    }
}

const MyPostsContainer = connect(mapStateToProps, mapDispatchToProps)(MyPosts)
export default MyPostsContainer;