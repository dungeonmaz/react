import classes from './Post.module.css'

const Post = (props) => {
    return (
        <div className={classes.item}>
            <div className={classes.imageLike}>
                <img src='https://thumbs.dreamstime.com/b/illustration-profile-icon-avatar-inhabitant-male-illustration-profile-icon-avata-237916010.jpg'></img>
                {props.likes}
                <button>+</button>
            </div>
            <div className={classes.text}>
                {props.message}
            </div>
        </div>
    )
}


export default Post;