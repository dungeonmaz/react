import classes from './DialogItem.module.css'
import { NavLink } from 'react-router-dom'

const DialogItem = (props) => {
    return (
        <div className={classes.dialog}>
            <NavLink to={'/dialogs/' + props.id} style={({ isActive }) => ({
                color: isActive ? 'gold' : ''
            })}>{props.name}</NavLink>
        </div>
    )
}

export default DialogItem;