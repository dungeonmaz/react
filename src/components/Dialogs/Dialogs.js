import classes from './Dialogs.module.css'
import DialogItem from './DialogItem/DialogItem.js'
import MessageItem from './MessageItem/MessageItem'
import { createRef } from 'react'


const Dialogs = (props) => {

    let dialogsElements = props.dialogsPage.dialogs.map(dialog => <DialogItem id={dialog.id} name={dialog.name} />)

    let messagesElements = props.dialogsPage.messages.map(message => <MessageItem text={message.text} />)

    let onAddMessage = () => {
        props.addMessage()
    }

    let newMessageElement = createRef()

    let onMessageChange = () => {
        let text = newMessageElement.current.value
        props.changeNewMessageText(text)
    }


    return (
        <div className={classes.dialogs}>
            <div className={classes.dialogsItems}>
                {dialogsElements}
            </div>

            <div className={classes.messages}>
                {messagesElements}
            </div>
            <textarea ref={newMessageElement} value={props.dialogsPage.newMessageText} onChange={onMessageChange} />
            <button onClick={onAddMessage}>Send Message</button>
        </div>
    )

}

export default Dialogs;