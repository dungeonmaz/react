import { connect } from 'react-redux'
import { addMessageActionCreator, changeNewMessageTextActionCreator } from '../../redux/dialogsReducer'
import Dialogs from './Dialogs'


let mapStateToProps = (state) => {
    return {
        dialogsPage: state.dialogsPage,
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        changeNewMessageText: (text) => {
            let action = changeNewMessageTextActionCreator(text)
            dispatch(action)
        },
        
        addMessage: () => {
            let action = addMessageActionCreator()
            dispatch(action)
        },
    }
}

const DialogsContainer = connect(mapStateToProps, mapDispatchToProps)(Dialogs);
export default DialogsContainer;